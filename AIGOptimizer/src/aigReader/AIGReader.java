package aigReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import graphRepresentation.AIG;
import graphRepresentation.AndNode;

public class AIGReader {

	public static AIG readBinaryAIG(String filename) throws IOException {
		
		LinkedHashSet<AndNode> nodes = new LinkedHashSet<>();
		AIG aig = new AIG(nodes);
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(filename)));

			String line = br.readLine();
			String[] header = line.split(" ");
			if (!line.contains("aig")) {
				System.err.println("Unsupported File Format! Use binary Aiger files for import");
				br.close();
				return null;
			}
			int m = Integer.parseInt(header[1]);
			int i = Integer.parseInt(header[2]);
			int l = Integer.parseInt(header[3]);
			int o = Integer.parseInt(header[4]);
			int a = Integer.parseInt(header[5]);

			LinkedList<Integer> deltas = reduceFileToByteListWithoutCommentsAndHeader(filename);

			generateNodes(aig, m, i, l, o, a, deltas);
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return aig;

	}

	private static LinkedList<Integer> reduceFileToByteListWithoutCommentsAndHeader(String filename)
			throws IOException {
		Path path = Paths.get(filename);
	    byte[] data = Files.readAllBytes(path);
	    LinkedList<Integer> deltas = new LinkedList<>();
	    int lines = 0;
	    for(int i=0; i<data.length;i++) {
	    	if(lines>=2) {
	    		if(data[i]==99 &&data[i+1] == 10 && data[i+2] == 101 && data[i+3] == 120 && data[i+4] == 0) {
	    			break;
	    		}
	    		deltas.add(Byte.toUnsignedInt(data[i]));
	    	}
	    	if(Byte.toUnsignedInt(data[i])==10) {
	    		lines++;
	    	}
	    }
	    return deltas;
	}

	private static void generateNodes(AIG aig, int m, int i, int l, int o, int a, LinkedList<Integer> bytes)
			throws NumberFormatException, IOException {
		if (m == i + l + a) {
			for (int k = 1; k <= i; k++) {
				aig.addNode(new AndNode(k, null, null, null, false, false));
			}
			for (int k = i + l + 1; k <= m; k++) {
				LinkedList<Integer> delta0 = new LinkedList<Integer>();
				int next = bytes.pop();
				delta0.push(next);
				if (bytes.isEmpty()) {
					break;
				}
				while (next > 127) {
					next = bytes.pop();
					delta0.addLast(next);
					
				}
				next = bytes.pop();

				LinkedList<Integer> delta1 = new LinkedList<Integer>();
				delta1.push(next);
				while (next > 127) {
					next = bytes.pop();
					delta1.addLast(next);
					
				}
				int idx0 = calcNodeIndexFromDelta(delta0, k,false);
				int idx1 = calcNodeIndexFromDelta(delta1, idx0,isFirstInputInverted(delta0,k)); // to calculate the index of the node we need to know the total value ( +1 if inverted) of the other input rx0 and rx1 include the inversion

				AndNode in0 = aig.findNodeWithIndex(idx0);
				AndNode in1 = aig.findNodeWithIndex(idx1);
				AndNode newNode = new AndNode(k, in0, in1, null, isFirstInputInverted(delta0,k), isSecondInputInverted(delta1,delta0,k));
				aig.addNode(newNode);
				in0.addOutput(newNode);
				in1.addOutput(newNode);
				if(k==m) {
					aig.setOutput((AndNode)newNode);
				}
			}
		} else
			System.err.println("Header definition is not correct!");
	}

	private static boolean isFirstInputInverted(LinkedList<Integer> delta0, int k) {  // you can�t know only with a delta if an input is inverted or not (see comment in calcNodeIndexFromDelta below). You need k for delta0 and delta0 and k for delta1
		if ((2*k - byteListToInt(delta0)) % 2 == 1) 
			return true;
		else
			return false;
	}
	
	private static boolean isSecondInputInverted(LinkedList<Integer> delta1, LinkedList<Integer> delta0, int k) {  // you can�t know only with a delta if an input is inverted or not (see comment in calcNodeIndexFromDelta below). Last Reference is k for delta0 and the other input for delta1
		if ((2*k - byteListToInt(delta0) - byteListToInt(delta1)) % 2 == 1) 
			return true;
		else
			return false;
	}

	private static int calcNodeIndexFromDelta(LinkedList<Integer> delta, int k, boolean inverted) { 
		return (2 * k + (inverted ? 1 : 0) - byteListToInt(delta)) / 2; // If you get something comma 5 (and then round), then it is inverted. You can check it here or like above. 
	}

	private static int byteListToInt(LinkedList<Integer> delta) {
		int result = 0;
		int power = 0;
		for (int b : delta) {
			if (b > 127) {
				b = b - 128;
			}
			result = (int) (result + b * Math.pow(128, power));
			power++;
		}
		return result;
	}
}
