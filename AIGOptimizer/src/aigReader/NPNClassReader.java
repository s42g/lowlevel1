package aigReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

import graphRepresentation.AIG;
import graphRepresentation.AndNode;

public class NPNClassReader {

	public static final int a = '0';
	public static final int b = '1';
	public static final int c = '2';
	public static final int d = '3';

	public static HashMap<String, TreeSet<AIG>> readNPNClassFromFile(String filename) {
		HashMap<String, TreeSet<AIG>> npnClasses = new HashMap<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
			String line = br.readLine();
			String index = "";
			TreeSet<AIG> functions = new TreeSet<>();
			int id = 0;
			while (line != null) {
				while (line.equals("")) {
					line = br.readLine();
				}

				if (line.contains("Class")) {
					index = parseHeader(line);
					line = br.readLine();
					functions = new TreeSet<>();
				}

				while (line != null && line.contains(" : 0x")) {
					// parse function line
					functions.add(parseFunctionLine(line,id));
					id++;
					line = br.readLine();
				}
				npnClasses.put(index, functions);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return npnClasses;
	}

	private static AIG parseFunctionLine(String line, int id) {
		String[] splittedLine = line.split(" ");
		AIG a = buildGraphFromString(splittedLine[splittedLine.length - 1]);
		a.setId(id);
		return a;
	}

	private static AIG buildGraphFromString(String string) {
		AIG graph = new AIG();

		if (string.contains("a"))
			graph.addInput(new AndNode(a-48, null, null, null, false, false));
		if (string.contains("b"))
			graph.addInput(new AndNode(b-48, null, null, null, false, false));
		if (string.contains("c"))
			graph.addInput(new AndNode(c-48, null, null, null, false, false));
		if (string.contains("d"))
			graph.addInput(new AndNode(d-48, null, null, null, false, false));

		char idCounter = '4';
		AndNode node = null;
		while (string.contains("(")) {
			// letzte �ffnende Klammer suchen
			int lastOpeningBracket = string.lastIndexOf("(");
			// erste schlie�ende Klammer nach der letzten �ffnenden Klammer suchen
			int firstClosingBracket = string.indexOf(")", lastOpeningBracket);
			// substring auslesen und ersetzen mit idcounter
			String substring = string.substring(lastOpeningBracket, firstClosingBracket + 1);
			string = string.replace(substring, String.valueOf(idCounter));
			// substring in Knoten umwandeln
			node = substringToNode(substring, idCounter, graph);
			graph.addNode(node);
			node.getInput1().addOutput(node);
			node.getInput2().addOutput(node);
			idCounter++;
		}
		if (string.length() > 1) {
			string = "(" + string + ")";
			// verbleibende Knoten zusammenfassen
			node = substringToNode(string, idCounter, graph);
			graph.addNode(node);
			node.getInput1().addOutput(node);
			node.getInput2().addOutput(node);
			idCounter++;
		}
		graph.setOutput(node);
		return graph;
	}

	private static AndNode substringToNode(String substring, int idCounter, AIG graph) {
		char char1 = substring.charAt(1);
		char char2;
		boolean inverted1 = false;
		boolean inverted2 = false;
		Character cInv = '\'';
		Character c2 = substring.charAt(2);
		if (c2.equals(cInv)) {
			inverted1 = true;
			char2 = substring.charAt(3);
			Character c4 = substring.charAt(4);
			if (c4.equals(cInv))
				inverted2 = true;
		} else {
			char2 = substring.charAt(2);
			Character c3 = substring.charAt(3);
			if (c3.equals(cInv))
				inverted2 = true;
		}
		int id1 = -1;
		int id2 = -1;
		if (char1 == 'a' || char1 == 'b' || char1 == 'c' || char1 == 'd') {
			if (char1 == 'a')
				id1 = a;
			else if (char1 == 'b')
				id1 = b;
			else if (char1 == 'c')
				id1 = c;
			else if (char1 == 'd')
				id1 = d;
		} else {
			id1 = char1; // funktioniert das???
		}
		if (char2 == 'a' || char2 == 'b' || char2 == 'c' || char2 == 'd') {
			if (char2 == 'a')
				id2 = a;
			else if (char2 == 'b')
				id2 = b;
			else if (char2 == 'c')
				id2 = c;
			else if (char2 == 'd')
				id2 = d;
		} else {
			id2 = char2; // funktioniert das???
		}
		AndNode in1 = null;
		AndNode in2 = null;
		Iterator<AndNode> it = graph.getNodes().iterator();
		while (it.hasNext()) {
			AndNode node = it.next();
			if (node.getId() == id1-48)
				in1 = node;
			if (node.getId() == id2-48)
				in2 = node;
		}
		if (in1 == null || in2 == null) {
			in1.toString();
		}
		HashSet<AndNode> out = new HashSet<>();
		AndNode newAndNode = new AndNode(idCounter-48, in1, in2, out, inverted1, inverted2);
		return newAndNode;
	}

	private static String parseHeader(String line) {
		String[] splittedLine = line.split(" ");
		return splittedLine[splittedLine.length - 1];
	}
}
