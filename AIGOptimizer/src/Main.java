import java.io.IOException;
import java.util.HashMap;
import java.util.TreeSet;

import aigReader.AIGReader;
import aigReader.NPNClassReader;
import aigRewriter.AIGRewriter;
import graphRepresentation.AIG;

public class Main {
	public static void main(String[] args) throws IOException {
		double totalbefore = 0;
		double total = 0;
		for(int i = 0; i<10; i++) {
			//long start = System.currentTimeMillis();
			
			String aigerName = "aig_"+i;
			AIG aig = AIGReader.readBinaryAIG("./aigers/"+aigerName+".aig");
			//AIG aig2 = AIGReader.readBinaryAIG("./aigers/test_0.aig");
			aig.setName(aigerName+"_"+"input");
			aig.toAigerCode();
			aig.toGraphvid();
			totalbefore+=aig.size();
			HashMap<String, TreeSet<AIG>> npn = NPNClassReader
					.readNPNClassFromFile("./aigers/abc_graph_lib_no_exor.txt");
			
			AIG aig3;
			System.out.print("File: aig_"+i+".aig Size: "+aig.size());
			//aig.toGraphvid();
			aig3= AIGRewriter.rewrite(aig, npn,true);
			System.out.println(" New size after Rewrite: "+aig3.size());
			total+=aig3.size();
			aig3.setName(aigerName+"_"+"output");
			aig3.toAigerCode();
			aig.toGraphvid();
			//long finish = System.currentTimeMillis();
			//long timeElapsed = finish - start;
			//System.out.println(" Time Elapsed:"+timeElapsed);

		}
		System.out.println(" Average Nodes: "+total/10);
		System.out.println(" Absolute: "+(total/totalbefore));

			
		
		
		
		// aig2.optimize(npn);
		
	}
}
