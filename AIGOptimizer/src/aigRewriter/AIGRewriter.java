package aigRewriter;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import graphRepresentation.AIG;
import graphRepresentation.AndNode;
import graphRepresentation.Node;

public class AIGRewriter {

	public static AIG rewrite(AIG graph, HashMap<String, TreeSet<AIG>> npn, boolean useZeroCost) {
		TreeMap<AndNode, TreeSet<Cut>> tableOfCuts = exhaustiveCutEnumeration(graph);
		graph.topologicalSorting();
		//graph.toGraphvid();
		String[] tt = null;
		int currentId = graph.getOutput().getId();
		while(currentId > graph.I) {
			AndNode eKey = graph.getNode(currentId);
			int [] bestInputOrder = new int[4];
			AIG bestCandidate = null;
			Cut bestCut = null;
			String inverseOfBest= "";
			int graphPreSize = graph.size();
			int bestGain =-1;
			
			if(eKey!=null) {
				for(Cut cut: tableOfCuts.get(eKey)) {
					//Get the canonocal form of the fuction
					tt = getCanonicalForm(cut.getLeaves(), (AndNode) eKey);
					
					//System.out.println("Canonical: "+tt[0]+" for node: "+eKey.getId()+"  and order: "+tt[1]+" and Nodes Inverted: "+tt[2]+ " and Output Inversion: "+tt[3]);
					if(!isCutValid(eKey, cut)) {
						//System.out.println("Cut not valid, One of the And Nodes inside the subgraph is connected with outside the subgraph! That dependency cannot be represented with any candidate. Jump...");
						continue;
					}
					
					String [] inputOrderString = tt[1].split(";");
					for(String inverted: tt[2].split(";")) {
						for(int i=0; i<inputOrderString.length;i++) {
							if(inputOrderString[i].compareTo(inverted)==0) {
								inputOrderString[i]="-"+inputOrderString[i];
							}
						}
					}
					int [] inputOrder = new int[4];
					for(int i=0; i<4; i++) {
						inputOrder[i] = Integer.parseInt(inputOrderString[i]);
					}
					//Compare with the npn classes and get the class
					TreeSet<AIG> candidates = npn.get(tt[0]); //There isnt always one.
					if(candidates==null) {
						//System.out.println("No candidates found for function "+tt[0]);
					}
					else {
					for(AIG candidate : candidates) {
							AIG save = new AIG();
							//To adapt Cut, we make a copy, not to change the original ones
							AIG candidateToUse = createSubgraphFromCut(candidate.getInputs(), candidate.getOutput());
							adaptCut(inputOrder, candidateToUse, Integer.parseInt(tt[3]), eKey);
							substituteCutWithAIG(eKey, graph,candidateToUse, save, cut,tt[3]);
							renameNoInputs(candidateToUse, graph);

							int gain = graphPreSize-graph.size();
							if(gain>0||(gain==0&&useZeroCost)) {
								if(bestCandidate==null||bestGain<gain) {
									bestCandidate = candidate;
									bestGain = gain;
									
									for(int i=0; i<4;i++) {
										bestInputOrder[i]=inputOrder[i];
									}
									bestCut = cut;
									inverseOfBest=tt[3];
								}
							}
							//Restore Graph to make the next substitution
							
							restoreGraph(graph, save, candidateToUse,tt[3]);
							graph.calculateOutput();
							
						}
						
						

					}
					
				}
				if(bestCandidate == null) {
					currentId--;
					continue;
				}
				//Make the best substitution
				AIG save = new AIG();
				AIG candidateToUse = createSubgraphFromCut(bestCandidate.getInputs(), bestCandidate.getOutput());
				adaptCut(bestInputOrder, candidateToUse, Integer.parseInt(inverseOfBest),eKey);
				substituteCutWithAIG(eKey, graph,candidateToUse, save, bestCut,inverseOfBest);
				renameNoInputs(candidateToUse, graph);
				graph.calculateOutput();
				
				renameNodesTopological(graph,candidateToUse.getOutput());
				graph.toGraphvid();
				tableOfCuts = exhaustiveCutEnumeration(graph);
				
			}
			
			currentId--;
		}
		return graph;
		
	}
	
	private static void renameNodesTopological(AIG g, AndNode eKey) {
		int top = eKey.getId();
		for(AndNode n: g.getNodes()) {
			if(n.getInput1()!=null&&n!=eKey&&n.getId()<=eKey.getId()) {
				n.setId(0);
			}
		}
		renameNodesTopologicalRecursive(g, g.getOutput(),top-1);
		
	}

	private static void renameNodesTopologicalRecursive(AIG g, AndNode n, int pos) {
		LinkedList<AndNode> list = new LinkedList<>();
		list.add(n.getInput1());
		list.add(n.getInput2());
		while(!list.isEmpty()) {
			if(list.get(0).getInput1()!=null) {
				if(list.get(0).getId()==0) {
					list.get(0).setId(pos);
					pos--;
				}
				list.add(list.get(0).getInput1());
				list.add(list.get(0).getInput2());

			}
			list.pop();
			
		}
	
	}
	private static boolean isCutValid(AndNode output,Cut c) {
		HashSet<AndNode> toRemove = new HashSet<>();
		toRemove=enumerateNodesRecursive(output,c);
		//toRemove.removeAll(c.getLeaves());
		for(AndNode n: toRemove) {
			if(!c.getLeaves().contains(n)&&n!=output) {
				for(AndNode out: n.getOutput()) {
					if(!toRemove.contains(out)) {
						//System.out.println("Node outside: "+out.getId());
						return false;
					}
				}
			}
		}
		toRemove.removeAll(c.getLeaves());
		toRemove.remove(output);
		for(AndNode n: c.getLeaves()) {
			if(n.getInput1()!=null&&(toRemove.contains(n.getInput1())||toRemove.contains(n.getInput2()))) {
				return false;
			}
		}
		return true;
		
	}
	
	
	
	private static HashSet<AndNode> enumerateNodesRecursive(AndNode node,Cut c) {
		HashSet<AndNode> toReturn = new HashSet<AndNode>();
		if(node.getInput1()==null||c.getLeaves().contains(node)){
			toReturn.add(node);
			return toReturn;
		}else
		{
			toReturn.addAll(enumerateNodesRecursive(node.getInput1(),c));
			toReturn.addAll(enumerateNodesRecursive(node.getInput2(),c));
			toReturn.add(node);
			return toReturn;
		}
		
	}
	

	public static void renameNoInputs(AIG candidate, AIG g) {
		int max=0;
		max = candidate.getOutput().getId();
		renameNoInputsRecursive(candidate, g,candidate.getOutput(),max);
	}
	
	public static int renameNoInputsRecursive(AIG candidate, AIG g, AndNode n,int pos) {
		if(candidate.getInputs().contains(n)) {
			return pos;
		}
		if(candidate.getOutput()!=n) {
			n.setId(pos);
			pos = renameNoInputsRecursive(candidate,g,n.getInput1(),pos);
			pos = renameNoInputsRecursive(candidate,g,n.getInput2(),pos);
			return pos;
		}
		pos = renameNoInputsRecursive(candidate,g,n.getInput1(),pos);
		pos = renameNoInputsRecursive(candidate,g,n.getInput2(),pos);
		return pos;
		
	}
	

	
	private static void restoreGraph(AIG graph, AIG save, AIG candidate, String invert) {
		for(AndNode n: save.getInputs()) {
			AndNode toSubstitute = graph.getNode(n.getId());
			if(toSubstitute.getInput1() != null) {
				//n.setInputs(toSubstitute.getInput1(),toSubstitute.getInput2());
				n.getInput1().addOutput(n);
				n.getInput2().addOutput(n);
				toSubstitute.getInput1().removeOutput(toSubstitute);
				toSubstitute.getInput2().removeOutput(toSubstitute);
				n.getInput1().getOutput().removeAll(candidate.getNodes());
				n.getInput2().getOutput().removeAll(candidate.getNodes());
				
			}
			
			for(AndNode out: toSubstitute.getOutput()) {
				if(out.getInput1() == toSubstitute) {
					out.setInput1(n);
				}
				else if (out.getInput2() == toSubstitute) {
					out.setInput2(n);
				}
				else {
					System.out.print("ERROR. CHECK!!!");
					throw new EmptyStackException();
				}
			}
			HashSet<Integer> toChange = new HashSet<>();
			for(AndNode out: n.getOutput()) {
				if (save.getInput(out.getId())!=null) {
					toChange.add(out.getId());
				}
				if(!save.getNodes().contains(out)&&out.getInput1()!=null) {
					if(out.getInput1()==n||out.getInput1()==toSubstitute) {
						out.setInput1(n);
					}
					else if (out.getInput2()==n||out.getInput2()==toSubstitute){
						out.setInput2(n);
					}
					else {
						System.out.print("ERROR. CHECK!!!");
					}
				}
			}
			for(int i:toChange) {
				n.addOutput(save.getNode(i));
				n.removeOutput(candidate.getNode(i));
			}
			
		}
		AndNode toSubstituteOutput = candidate.getOutput();
		save.getOutput().setOutput(toSubstituteOutput.getOutput());
		for(AndNode out: toSubstituteOutput.getOutput()) {
			if(out.getInput1() == toSubstituteOutput) {
				out.setInput1(save.getOutput());
			}
			else if((out.getInput2() == toSubstituteOutput)) {
				out.setInput2(save.getOutput());
			}
			else {
				System.out.print("ERROR. CHECK!!!");
			}
			
		}
		
		if(invert=="1") {
			
			for(AndNode outPutOfOutput :save.getOutput().getOutput()) {
				if(outPutOfOutput.getInput1() == save.getOutput()){
					outPutOfOutput.setInverted1(!outPutOfOutput.isInverted1());
				}
				else if (outPutOfOutput.getInput2() == save.getOutput()){
					outPutOfOutput.setInverted2(!outPutOfOutput.isInverted2());
				}
				else {
					System.out.print("ERROR. CHECK!!!");
				}
			}
		}

		graph.removeNodes(candidate.getNodes());
		
		for(AndNode n: save.getNodes()) {
			graph.addNode(n);
		}
	}
	
	public static void substituteCutWithAIG(AndNode output, AIG principalGraph, AIG candidate, AIG save, Cut c, String invertedOutput) {
		removeCutFromAIG(principalGraph,output,c,save);
		for(AndNode input: candidate.getInputs()) {
			AndNode copyFrom = save.getNode(input.getId());
			input.setInputs(copyFrom.getInput1(), copyFrom.getInput2());
			if(copyFrom.getInput1()!=null) {
				int i1 = copyFrom.getInput1().getId();
				if(candidate.getNode(i1)!=null&&candidate.getInput(i1)!=null) {
					input.setInput1(candidate.getInput(copyFrom.getInput1().getId()));
				}
				int i2 = copyFrom.getInput2().getId();
				if(candidate.getNode(i2)!=null&&candidate.getInput(i2)!=null) {
					input.setInput2(candidate.getInput(copyFrom.getInput2().getId()));
				}
			}
				
			input.setInverted1(copyFrom.isInverted1());
			input.setInverted2(copyFrom.isInverted2());
			if(copyFrom.getInput1() != null) {
				copyFrom.getInput1().addOutput(input);
				copyFrom.getInput2().addOutput(input);
				copyFrom.getInput1().removeOutput(copyFrom);
				copyFrom.getInput2().removeOutput(copyFrom);
				
			}
			HashSet<Integer> toChange = new HashSet<>();
			for(AndNode out: copyFrom.getOutput()) {
				if (save.getInput(out.getId())!=null) {
					toChange.add(out.getId());
				}
				if(!save.getNodes().contains(out)) {
					if (save.getInput(out.getId())!=null) {
						toChange.add(out.getId());
					}
					if((out.getInput1() == copyFrom)||out.getInput1()==input) {
						out.setInput1(input);
					}
					else{
						out.setInput2(input);
					}
					input.addOutput(out);
				}
			}
			for(int i:toChange) {
				input.addOutput(candidate.getNode(i));
				input.removeOutput(save.getNode(i));
			}
		}
		candidate.getOutput().setOutput(output.getOutput());
		
		for(AndNode out: output.getOutput()) {
			if(out.getInput1() == output) {
				out.setInput1(candidate.getOutput());
			}
			else {
				out.setInput2(candidate.getOutput());
			}
		}
		if(invertedOutput.equals("1")) {
			HashSet<AndNode> outputOutputs = candidate.getOutput().getOutput();
			for(AndNode outputOutput:outputOutputs) {
				if(outputOutput.getInput1() == candidate.getOutput()) {
					outputOutput.setInverted1(!outputOutput.isInverted1());
				}else {
					outputOutput.setInverted2(!outputOutput.isInverted2());
				}
			}
		}
		for(AndNode n: candidate.getNodes()) {
			principalGraph.addNode(n);
		}
		
		
	}
	
	
	public static void adaptCut(int[] inputOrder, AIG candidate, int totalInversion, AndNode outputInGraph) {
		AndNode [] candidateInputs = new AndNode[4];
		
		for (int i=0; i<4; i++) {
			candidateInputs[i] = candidate.getNode(i);
		}
		for (int i=0; i<4; i++) {
			candidateInputs[i].setId(Math.abs(inputOrder[3-i]));
			if(inputOrder[3-i]<0) {
				for(AndNode output: candidateInputs[i].getOutput()) {
					if(output.getInput1() == candidateInputs[i]) {
						output.setInverted1(!output.isInverted1());
					}
					if(output.getInput2() == candidateInputs[i]) {
						output.setInverted2(!output.isInverted2());
					}
				}
			}
		}
		int newName = Integer.MAX_VALUE;
		for(AndNode n: candidate.getNodes()) {
			if(!candidate.getInputs().contains(n)&&candidate.getOutput()!=n) {
				n.setId(newName);
				newName-=1;
			}
		}
		candidate.getOutput().setId(outputInGraph.getId());
	}
	
	
	
	public static void removeCutFromAIG(AIG g, AndNode output, Cut c, AIG save) {
		removeCutRecursive(g,output,c,save,0);
	}
	
	public static void removeCutRecursive(AIG g, AndNode output, Cut c, AIG save, int deep) {
		if(c.getLeaves().contains(output)){ 
			save.addNode(output);
			g.removeNode(output);
			save.addInput(output);
			return;
		}
		removeCutRecursive(g,((AndNode)output).getInput1(),c,save,deep-1);
		removeCutRecursive(g,((AndNode)output).getInput2(),c,save,deep-1);
		save.addNode(output);
		g.removeNode(output);
		if(deep == 0) {
			save.setOutput((AndNode) output);
		}
		
	}
	
	//Tries all possible combinations for the inputs and gets the truth table. needs topological order to work
	//D-C-B-A (first,second,third,fourth)
	public static String evaluateTruthTableOfCut(Node first,Node second, Node third, Node fourth, AndNode output,Boolean print) {
		int a=0;
		int b=0;
		int c=0;
		int d=0;
		HashMap<Node,Integer> inputsMap = new HashMap<>();
		HashSet<Node> cutInputs = new HashSet<>();
		cutInputs.add(first);
		cutInputs.add(second);
		cutInputs.add(third);
		cutInputs.add(fourth);
		String tt = "";
		for(;a<2;a++) {
			b=0;
			for(;b<2;b++) {
				c=0;
				for(;c<2;c++) {
					d=0;
					for(;d<2;d++) {
						inputsMap.put(first, a);
						inputsMap.put(second, b);
						inputsMap.put(third, c);
						inputsMap.put(fourth, d);
						
						int tt1 = evaluateRecursive(cutInputs, output,inputsMap);
						tt= tt1+tt;
						if(print) {
							System.out.print(a+" "+b+" "+c+" "+d);
							System.out.println(" " +tt1);
						}
						
					}
				}
			}
		}
		
		return tt;
	}
	
	public static int evaluateRecursive(HashSet<Node> cutInputs, Node node, HashMap<Node,Integer> inputsMap) {
		
		if(cutInputs.contains(node)){ //hier!!!
			return inputsMap.get(node);
		}
		int m1 =  ((evaluateRecursive(cutInputs,((AndNode) node).getInput1(),inputsMap)+(((AndNode) node).isInverted1()?1:0)))%2;
		if(m1==0) {
			return m1;
		}
		int m2 =  ((evaluateRecursive(cutInputs,((AndNode) node).getInput2(),inputsMap)+(((AndNode) node).isInverted2()?1:0)))%2;
		
		return m1&m2;
	}
	
	public static String[] getCanonicalForm2 (HashSet<AndNode> cutInputs, AndNode output) {
		
		AIG subgraph = createSubgraphFromCut(cutInputs, output);
		boolean print=true;
		String canonicaltt[] = new String[4];

		canonicaltt[0]="1111111111111111";
		//N Equivalence
		for(int i=0; i<16; i++) {
			HashSet<Node> nodesInverted=new HashSet<>();
			String inversion = Integer.toBinaryString(i);
			while(inversion.length()<4) {
				inversion='0'+inversion;
			}
			HashSet<AndNode> inputsTemp = subgraph.getInputs();
			int [] inputsId = new int[4];
			int index=0;
			for(AndNode input: inputsTemp) {
				inputsId[index] = input.getId();
				index++;
			}
			Arrays.sort(inputsId);
			Node inputA = subgraph.getNode(inputsId[3]);// it.next();
			Node inputB= subgraph.getNode(inputsId[2]);// = it.next();
			Node inputC= subgraph.getNode(inputsId[1]);// = it.next();
			Node inputD= subgraph.getNode(inputsId[0]);// = it.next();
			
			for(int j=0; j<4; j++) {
				if(inversion.charAt(j)=='1') {
					nodesInverted.add(subgraph.getNode(inputsId[j]));
					for(Node outputOfInput: subgraph.getNode(inputsId[j]).getOutput()) {
						if(((AndNode)outputOfInput).getInput1() == subgraph.getNode(inputsId[j])) {
							((AndNode)outputOfInput).setInverted1(!((AndNode)outputOfInput).isInverted1());
						}
						else
						{
							((AndNode)outputOfInput).setInverted2(!((AndNode)outputOfInput).isInverted2());
						}
					}
				}
			}
			//P Equivalence
			
			
			
			Node[] inputs = new Node[4];
			for(int a=0; a<4;a++) {
				for(int b=0; b<4;b++) {
					if(a==b) {
						continue;
					}
					for(int c=0; c<4;c++) {
						if(c==b||c==a) {
							continue;
						}
						for(int d=0; d<4;d++) {
							if(d==c||d==b||d==a) {
								continue;
							}
							inputs[a]=inputA;
							inputs[b]=inputB;
							inputs[c]=inputC;
							inputs[d]=inputD;
							String tt = evaluateTruthTableOfCut(inputs[0],inputs[1],inputs[2],inputs[3], subgraph.getOutput(),print);
							int numberOfOnes = 0;
							for (int z = 0; z < tt.length(); z++) {
							    if (tt.charAt(z) == '1') {
							        numberOfOnes++;
							    }
							}
							String outputInversion="0";
							if(numberOfOnes>8) {
								outputInversion="1";
							    BigInteger twoToLength = new BigInteger("2").pow(tt.length());
							    tt = twoToLength.add(new BigInteger(tt, 2).not()).toString(2);
							    while(tt.length()<16) {
							    	tt='0'+tt;
							    }
							    //System.out.println(tt);
							}
							if(print) {
								System.out.println(tt);
								print=false;
							}
							if(Integer.parseInt(tt, 2)< Integer.parseInt(canonicaltt[0], 2)) {
								
								canonicaltt[0] = tt;
								canonicaltt[1] = ""+inputs[0].getId()+";"+inputs[1].getId()+";"+inputs[2].getId()+";"+inputs[3].getId(); //The IDs of the inputs reordered
								canonicaltt[2] = "";
								canonicaltt[3] = outputInversion;
								for(Node n: nodesInverted) {
									canonicaltt[2] = canonicaltt[2]+n.getId()+";"; 
								}
								//The Inversion of the reordered inputs
							}
						}
					}
				}
			}

			
			//reverse the changes
			for(Node input: subgraph.getInputs()) {
				for(Node outputOfInput: input.getOutput()) {
					if(nodesInverted.contains(input)) {
						if(((AndNode)outputOfInput).getInput1() == input) {
							((AndNode)outputOfInput).setInverted1(!((AndNode)outputOfInput).isInverted1());
						}
						else
						{
							((AndNode)outputOfInput).setInverted2(!((AndNode)outputOfInput).isInverted2());
						}
				
					}
				}
			}
			
			
			
			
		}
		
		
		return canonicaltt;
		
		
		
	}
	
	public static String[] getCanonicalForm(HashSet<AndNode> cutInputs, AndNode output) {
		
		AIG subgraph = createSubgraphFromCut(cutInputs, output);
		HashSet<AndNode> inputsTemp = subgraph.getInputs();
		int [] inputsId = new int[4];
		int index=0;
		for(AndNode input: inputsTemp) {
			inputsId[index] = input.getId();
			index++;
		}
		Arrays.sort(inputsId);
		Node inputA = subgraph.getNode(inputsId[0]);
		Node inputB= subgraph.getNode(inputsId[1]);
		Node inputC= subgraph.getNode(inputsId[2]);
		Node inputD= subgraph.getNode(inputsId[3]);
		Node[] inputs = new Node[4];
		inputs[0]=inputA;
		inputs[1]=inputB;
		inputs[2]=inputC;
		inputs[3]=inputD;
		String []canonical= new String[4];
		String ttFirst= evaluateTruthTableOfCut(inputD,inputC,inputB,inputA, subgraph.getOutput(), false);
		int ones =0;
		boolean invertOutput = false;
		for(int i=0; i<16; i++) {
			if(ttFirst.charAt(i)=='1') {
				ones+=1;
			}
		}
		if(ones>=8) {
			invertOutput=true;
		}
		canonical[0] = "1111111111111111";
		for(int a=0; a<4;a++) {
			for(int b=0; b<4;b++) {
				if(a==b) {
					continue;
				}
				for(int c=0; c<4;c++) {
					if(c==b||c==a) {
						continue;
					}
					for(int d=0; d<4;d++) {
						if(d==c||d==b||d==a) {
							continue;
						}
						
						for(int i=0; i<16; i++) {
							HashSet<AndNode> inverted = new HashSet<>();
							String inversion = Integer.toBinaryString(i);
							while (inversion.length()<4) {
								inversion="0"+inversion;
							}
							for(int j=0; j<4; j++) {
								if(inversion.charAt(j)=='1') {
									invertNode((AndNode) inputs[j]);
									inverted.add((AndNode) inputs[j]);
								}
							}
							String tt= evaluateTruthTableOfCut(inputs[d],inputs[c],inputs[b],inputs[a], subgraph.getOutput(), false);
							
							String [] ttt = new String[4];
							if(!invertOutput) {
								ttt[0]=tt;
								ttt[3]="0";
							}
							else {
								ttt[0]=tt;
								ttt[0]=ttt[0].replace('1', '2').replace('0', '1').replace('2', '0');
								ttt[3]="1";
							}
							
							
							if(Long.parseLong(ttt[0])<Long.parseLong(canonical[0])) {
								canonical[0]=ttt[0];
								canonical[1]=inputs[d].getId()+";"+inputs[c].getId()+";"+inputs[b].getId()+";"+inputs[a].getId();
								canonical[2]="";
								for(AndNode in:inverted) {
									canonical[2]=in.getId()+";"+canonical[2];
								}
								canonical[3]=ttt[3];
							}
							for(int j=0; j<4; j++) {
								if(inversion.charAt(j)=='1') {
									invertNode((AndNode) inputs[j]);
									inverted.add((AndNode) inputs[j]);
								}
							}
						}
					}
				}
			}
		}
		return canonical;
	}
	
	private static void invertNode(AndNode n) {
		for(AndNode out: n.getOutput()) {
			if(out.getInput1()==n) {
				out.setInverted1(!out.isInverted1());
			}
			else {
				out.setInverted2(!out.isInverted2());
			}
		}
		
	}
	public static AIG createSubgraphFromCut(HashSet<AndNode> cutInputs, AndNode output) {
		AIG g = new AIG();
		createSubgraphFromCutRecursive(cutInputs, output, g, 0);
		return g;
	}
	
	private static AndNode createSubgraphFromCutRecursive(HashSet<AndNode> cutInputs, AndNode output,AIG g, int id) {
		AndNode copy;
		
		if(output.getInput1() == null) {
			if(!g.containsNode(output.getId())) {
				copy = new AndNode(output.getId(), null, null, null, false, false);
				g.addNode(copy);
				g.addInput(copy);
				return copy;
			}
			else
			{
				return g.getNode(output.getId());
			}
		}
		if(cutInputs.contains(output)) {
			if(!g.containsNode(output.getId())) {
				copy = new AndNode(output.getId(), null, null, null, false, false);
				g.addNode(copy);
				g.addInput(copy);
				return copy;
			}
			else {
				return g.getNode(output.getId());
			}
		}
		copy = new AndNode(output.getId(), null, null, null, ((AndNode) output).isInverted1(), ((AndNode) output).isInverted2());
		AndNode copyInput1 = createSubgraphFromCutRecursive(cutInputs, ((AndNode) output).getInput1(),g,id-1);	
		AndNode copyInput2 = createSubgraphFromCutRecursive(cutInputs, ((AndNode) output).getInput2(),g,id-1);	//id-2 falsch
		copyInput1.addOutput(copy);
		copyInput2.addOutput(copy);
		((AndNode) copy).setInputs(copyInput1, copyInput2);
		g.addNode(copy);
		if(id==0) {
			g.setOutput((AndNode) copy);
		}
		return copy;
	}

	
	
	@SuppressWarnings("unchecked")
	public static TreeMap<AndNode, TreeSet<Cut>> exhaustiveCutEnumeration(AIG graph) {
		
		HashMap<AndNode, Integer> ands = new HashMap<>(); // int = 2 means ready to be processed <2 not ready
		TreeMap<AndNode, TreeSet<Cut>> tableOfCuts = new TreeMap<>(); //each node has a set of possible cuts
		//set the nodes to be processed
		int cutId=0;
		for (AndNode n : graph.getNodes()) {
			if (n.getInput1()!=null) {
				int nOfVariableInputs = 0;
				if (((AndNode) n).getInput1().getInput1() == null) {
					nOfVariableInputs++;
				}
				if (((AndNode) n).getInput2().getInput1() == null) {
					nOfVariableInputs++;
				}
				ands.put((AndNode) n, nOfVariableInputs);
			}else { // if is a prime, put the corresponding cuts directly
				TreeSet<Cut> cutsOfPrime = new TreeSet<>();
				Cut cutOfSamePrime = new Cut();	
				cutOfSamePrime.setId(cutId);
				cutId++;
				cutOfSamePrime.addLeave(n);
				cutsOfPrime.add(cutOfSamePrime); //the 1- input cut (this same prime node)
				tableOfCuts.put(n, cutsOfPrime);
			}
		}
		while (!ands.isEmpty()) { // while there are some AND nodes to be processed
			Iterator<?> it = ands.entrySet().iterator();
			Entry<AndNode, Integer> entry = (Entry<AndNode, Integer>) it.next();
		
			
			while (entry.getValue() != 2) { // Take one AND Node whose inputs have already been processed
				if(!it.hasNext()) {
					System.out.println("ERROR!!!");
				}
				entry = (Entry<AndNode, Integer>) it.next();
			}
			AndNode currentNode = entry.getKey();
			TreeSet<Cut> cutsOfNode = new TreeSet<>();
			
			Cut cutOfSameNode = new Cut();
			cutOfSameNode.setId(cutId);
			cutId++;
			cutOfSameNode.addLeave(currentNode);
			

			cutsOfNode.add(cutOfSameNode); //the 1- input cut (this same node)
			
			HashSet<Cut> cutsInput1 = new HashSet<>();
			cutsInput1.addAll(tableOfCuts.get(currentNode.getInput1()));
			HashSet<Cut> cutsInput2 = new HashSet<>();
			cutsInput2.addAll(tableOfCuts.get(currentNode.getInput2()));
			for(Cut c1: tableOfCuts.get(currentNode.getInput1())){
				for(Cut c2: tableOfCuts.get(currentNode.getInput2())) {
					if(c2.sameAs(c1)) {
						
						Cut c3 = new Cut();
						c3.setId(cutId);
						cutId++;
						c3.addAllLeaves(c1.getLeaves());
						cutsOfNode.add(c3);
						
					}
				}
			}
				
			for(Cut c1: cutsInput1){
				for(Cut c2: cutsInput2) {
					Cut c3 = new Cut();
					c3.setId(cutId);
					cutId++;
					c3.addAllLeaves(c1.getLeaves());
					c3.addAllLeaves(c2.getLeaves());
					if(c3.size()<=4) {
						cutsOfNode.add(c3);
					}
				}
			}
			tableOfCuts.put(currentNode,cutsOfNode);
			
			
			
			for (Node n : currentNode.getOutput()) {
				ands.put((AndNode) n, ands.get(n) + 1);
			}
			ands.remove(currentNode);
		}
		
		//remove the != 4 cuts
		for(Entry<AndNode, TreeSet<Cut>> etOC:tableOfCuts.entrySet()) {
			HashSet<Cut> remainingCuts = new HashSet<>();
			remainingCuts.addAll(etOC.getValue());
			for(Cut c: remainingCuts) {
				if(c.size()!=4) {
					etOC.getValue().remove(c);
				}
			}
		}
		

		return tableOfCuts;
	}
	
	public static HashSet<AndNode> getAllAncesters(AndNode n){
		HashSet<AndNode> ancesters = getAllAncestersRecursive(n);
		ancesters.remove(n);
		return ancesters;
	}
	
	public static HashSet<AndNode> getAllAncestersRecursive(AndNode n){
		HashSet<AndNode> ancesters = new HashSet<>();
		if(n.getInput1()==null) {
			ancesters.add(n);
		}
		else {
			ancesters.addAll(getAllAncestersRecursive(((AndNode)n).getInput1()));
			ancesters.addAll(getAllAncestersRecursive(((AndNode)n).getInput2()));
			ancesters.add(n);
		}
		return ancesters;
	}
	
}
