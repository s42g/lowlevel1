package aigRewriter;

import java.util.HashSet;
import java.util.Iterator;

import graphRepresentation.AndNode;
import graphRepresentation.Node;

public class Cut implements Comparable<Cut> {
	
	private HashSet<AndNode> leaves;
	private int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Cut(HashSet<AndNode> leaves) {
		this.leaves = leaves;
	}
	public Cut() {
		leaves= new HashSet<>();
	}
	
	public void addLeave(AndNode n) {
		leaves.add(n);
	}
	

	public void addAllLeaves( HashSet<AndNode> l) {
		leaves.addAll(l);
	}
	
	public int size() {
		return leaves.size();
	}
	
	public HashSet<AndNode> getLeaves(){
		return leaves;
	}
	
	public boolean sameAs(Cut c) {
		int same = 0;
		for(Node n1:this.leaves) {
			for(Node n2:c.leaves) {
				if(n1 == n2) {
					same++;
				}
			}
		}
		if(same == this.size()&&same==c.size()) {
			return true;
		}
		return false;
	}
	
	
	
	public String toString() {
		Iterator<?> it = leaves.iterator();
		String s="[";
		for(int i=0;i<leaves.size()-1;i++) {
			s=s+((Node)it.next()).getId()+",";
		}
		s=s+((Node)it.next()).getId();
		s = s+"]";
		return s;
	}
	@Override
	public int compareTo(Cut o) {
		return -this.getId()+o.getId();
	}
}