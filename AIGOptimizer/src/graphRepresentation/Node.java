package graphRepresentation;

import java.util.HashSet;

public interface Node {

	void setOutput(HashSet<AndNode> nodes);

	void addOutput(AndNode node);

	HashSet<AndNode> getOutput();

	void setId(int id);

	int getId();

	String toString();

}
