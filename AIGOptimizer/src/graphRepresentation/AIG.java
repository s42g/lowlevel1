package graphRepresentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map.Entry;

import aigRewriter.AIGRewriter;
import aigRewriter.Cut;

public class AIG implements Comparable<AIG>{

	private String name;
	LinkedHashSet<AndNode> nodes;
	HashSet<AndNode> inputs;
	AndNode output;
	private final int RESOLUTION = 10;
	public int I;
	public int A;
	private int id;
	
	public HashSet<AndNode> getInputs() {
		return inputs;
	}

	public void addInput(AndNode input) {
		inputs.add(input);
		nodes.add(input);
	}

	public AndNode getOutput() {
		return output;
	}

	public void setOutput(AndNode output) {
		this.output = output;
		nodes.add(output);
	}

	public void removeNode(AndNode n) {
		nodes.remove(n);
		if(inputs.contains(n)) {
			inputs.remove(n);
		}
		if(n == output) {
			output = null;
		}
	}
	
	public void removeNodes(HashSet<AndNode> removes) {
		for(AndNode n:removes)
			removeNode(n);
	}
	
	public boolean containsNode(int id) {
		for(AndNode n: nodes) {
			if(n.getId() == id) {
				return true;
			}
		}
		return false;
	}
	
	public AndNode getNode(int id) {
		AndNode a = null;
		int found = 0;
		for(AndNode n: nodes) {
			if(n.getId() == id) {
				a = n;
				found++;
			}
		}
		if(found >1) {
			System.out.println("Error! more than one!");
			throw new EmptyStackException();
		}
		return a;
	}
	public AndNode getInput(int id) {
		for(AndNode n: inputs) {
			if(n.getId() == id) {
				return n;
			}
		}
		return null;
	}

	public AIG(LinkedHashSet<AndNode> nodes) {
		this.nodes = nodes;
		calculatePrimesAnds();
		this.inputs = new HashSet<AndNode>();

	}

	public AIG() {
		this.nodes = new LinkedHashSet<AndNode>();
		this.inputs = new HashSet<AndNode>();
	}

	public int size() {
		return nodes.size();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addNode(AndNode n) {
		nodes.add(n);
		calculatePrimesAnds();
	}
	

	public void writeAnd(int lhs, int rhs0, int rhs1) {
		System.out.printf("%d %d", lhs - rhs0, lhs - rhs1);
	}

	public void calculatePrimesAnds() {
		I=0;
		A=0;
		for (AndNode n : nodes) {
			if (n.getInput1()==null) {
				I++;
			} else if (n.getInput1()!=null) {
				A++;
			}
		}
	}
	
	public int getPrimesNum() {
		return I;
	}
	
	public int getAndsNum() {
		return A;
	}
	@SuppressWarnings("unchecked")
	public void toAigerCode() {

		int inputId = 2;
		
		topologicalSorting();
		calculatePrimesAnds();
		
		HashMap<AndNode, Integer> ands = new HashMap<>(); // int = 2 means ready to be written <2 not ready
		HashMap<AndNode, Integer> toRestore = new HashMap<>();
		
		for (AndNode n : nodes) {
			toRestore.put(n, n.getId());
			if (n.getInput1()==null ) {
				n.setId(n.getId()*2);
				inputId += 2;
			} else if (n.getInput1()!=null) {
				int nOfVariableInputs = 0;
				if (((AndNode) n).getInput1().getInput1()==null) {
					nOfVariableInputs++;
				}
				if (((AndNode) n).getInput2().getInput1()==null) {
					nOfVariableInputs++;
				}
				n.setId(0);
				ands.put((AndNode) n, nOfVariableInputs);
			}
		}
		int lhs = 2 * (I + 1); // First row after Header
		ArrayList<Integer> deltas = new ArrayList<Integer>();
		int deltaPosition = 0;

		while (!ands.isEmpty()) { // while there are some AND nodes to be processed
			Iterator<?> it = ands.entrySet().iterator();
			Entry<AndNode, Integer> entry = (Entry<AndNode, Integer>) it.next();
			boolean go = true;
			while (entry.getValue() != 2) { 
				if(!it.hasNext()) {
					System.out.println(inputId);
					System.out.println("entry: "+entry.getKey());

					System.out.println(this);
					
				}
					entry = (Entry<AndNode, Integer>) it.next();

			}
			if(go) {
				int rh1 = 0;
				int rh0 = 0;
	
				if (entry.getKey().getInput1().getId() < entry.getKey().getInput2().getId()) { // Because rh1 must be always
																								// smaller than rh0
					rh1 = entry.getKey().getInput1().getId() + (entry.getKey().isInverted1() ? 1 : 0);
					rh0 = entry.getKey().getInput2().getId() + (entry.getKey().isInverted2() ? 1 : 0);
				} else {
					rh0 = entry.getKey().getInput1().getId() + (entry.getKey().isInverted1() ? 1 : 0);
					rh1 = entry.getKey().getInput2().getId() + (entry.getKey().isInverted2() ? 1 : 0);
				}
	
				entry.getKey().setId(lhs);
				int delta0Acc = lhs - rh0;
				int delta1Acc = rh0 - rh1;
	
				boolean trigger0 = false;
				boolean trigger1 = false;
	
				// delta1
				for (int i = RESOLUTION; i > 0; i--) {
					int powComponent1 = (int) (delta1Acc / Math.pow(0x80, i));
					if (powComponent1 != 0 || trigger1) {
						if (trigger1) {
							deltas.add(deltaPosition, powComponent1 + 0x80);
						} else {
							deltas.add(deltaPosition, powComponent1);
							trigger1 = true;
						}
						// delta1[i]+=powComponent1;
						// delta1[i-1]+=0x80;
						delta1Acc -= powComponent1 * Math.pow(0x80, i);
					}
				}
				deltas.add(deltaPosition, delta1Acc + (trigger1 ? 0x80 : 0));
	
				// delta0
				for (int i = RESOLUTION; i > 0; i--) {
					int powComponent0 = (int) (delta0Acc / Math.pow(0x80, i));
					if (powComponent0 != 0 || trigger0) { // Trigger because if the Byte in position x is used, all Bytes
															// x-1, x-2 ... until 0 must be used, even if they have the
															// value 0, in which case they would be 0x80 (not 0x00)
						if (trigger0) {
							deltas.add(deltaPosition, powComponent0 + 0x80);
						} else {
							deltas.add(deltaPosition, powComponent0);
							trigger0 = true;
						}
						delta0Acc -= powComponent0 * Math.pow(0x80, i);
					}
				}
				deltas.add(deltaPosition, delta0Acc + (trigger0 ? 0x80 : 0));
				lhs += 2; // put at the bottom
				for (AndNode n : entry.getKey().getOutput()) {
					ands.put((AndNode) n, ands.get(n) + 1);
				}

				ands.remove(entry.getKey()); // AndNode processed
				deltaPosition = deltas.size(); // The next AND must be added at the end of the List
			}
			writeAigFile(A, I, deltas);
		}
		//restore the indices
		for(AndNode n: nodes) {
			n.setId(toRestore.get(n));
		}
	}

	public void writeAigFile(int A, int I, ArrayList<Integer> deltas) {



		File aigerFile = new File("aigers/" + this.name + ".aig");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(aigerFile);
			String s = "aig " + (I + A) + " " + I + " 0 1 " + A + "\n";
			fos.write(s.getBytes());
			s = 2 * (I + A) + "\n";
			fos.write(s.getBytes());
			// Writes bytes from the specified byte array to this file output stream
			for (int i : deltas) {
				fos.write(i);
			}
			fos.write('c');
			fos.write(10);
			fos.write('e');
			fos.write('x');
			fos.write(0);
			fos.write(10);

			Date date = new Date();
			s = "This file was written by AIGOptimizer on " + date.toString();
			fos.write(s.getBytes());
		} catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while writing file " + ioe);
		} finally {
			// close the streams using close method
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException ioe) {
				System.out.println("Error while closing stream: " + ioe);
			}

		}
	}
	
	@SuppressWarnings("unchecked")
	public void topologicalSorting() {
		
		HashMap<AndNode, Integer> ands = new HashMap<>(); // int = 2 means ready to be written <2 not ready
		HashSet<AndNode> primes = new HashSet<>(); // int = 2 means ready to be written <2 not ready
		
		
		while (!nodes.isEmpty()) {
			AndNode n = nodes.iterator().next();
			if (n.getInput1()!=null || n.getInput1() == null) {
				nodes.remove(n);
				primes.add((AndNode) n);
			} else if (n.getInput1()!=null) {
				int nOfVariableInputs = 0;
				if (((AndNode) n).getInput1().getInput1()!=null || ((AndNode)((AndNode)n).getInput1()).getInput1() == null) {
					nOfVariableInputs++;
				}
				if (((AndNode) n).getInput2().getInput1()!=null|| ((AndNode)((AndNode)n).getInput2()).getInput1() == null) {
					nOfVariableInputs++;
				}
				nodes.remove(n);
				ands.put((AndNode) n, nOfVariableInputs);
			}
			
		}
		
		nodes.addAll(primes);
		
		while(!ands.isEmpty()) {
			Iterator<?> it2 = ands.entrySet().iterator();
			Entry<AndNode, Integer> entry = (Entry<AndNode, Integer>) it2.next();
	
			while (entry.getValue() != 2) { // Take one AND AndNode whose inputs have already been processed
				entry = (Entry<AndNode, Integer>) it2.next();
			}
			//nodes.remove(entry.getKey());
			nodes.add(entry.getKey());
			
			for (AndNode n : entry.getKey().getOutput()) {
				ands.put((AndNode) n, ands.get(n) + 1);
			}
			
			ands.remove(entry.getKey());
		}
	}
	
	public void calculateOutput() {
		if(this.output==null) {
			for(AndNode n: this.nodes) {
				if(n.getOutput().size()==0) {
					this.output=n;
					return;
				}
			}
		}
	}
	

	public void replaceNode(AndNode oldAndNode, AndNode newAndNode) {
		nodes.remove(oldAndNode);
		nodes.add(newAndNode);
	};

	@Override
	public String toString() {
		String a = "";
		for (AndNode node : nodes) {
			a += node.toString() + "\n";
		}
		return a;
	}
	
	public LinkedHashSet<AndNode> getNodes(){
		return this.nodes;
	}

	public AndNode findNodeWithIndex(int i) {
		for (AndNode node : nodes) {
			if (node.getId() == i)
				return node;
		}
		return null;

	}
	
	public void toGraphvid() {
		
		
		File graphvid = new File("graphvid/" + this.name + ".txt");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(graphvid);
			String s = "digraph G {\n";
			fos.write(s.getBytes());
			
			for(AndNode n: nodes) {
				if(n.getInput1()!=null) {
					s = n.getId()+" -> "+n.getInput1().getId()+";\n";
					fos.write(s.getBytes());
				    s = n.getId()+" -> "+n.getInput2().getId()+";\n";
					fos.write(s.getBytes());
				}
				
			}
			s = "}\n";
			fos.write(s.getBytes());

		} catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while writing file " + ioe);
		} finally {
			// close the streams using close method
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException ioe) {
				System.out.println("Error while closing stream: " + ioe);
			}

		}
	}
	

	public void toGraphvid(Cut cut, AndNode output) {
		AIG a = AIGRewriter.createSubgraphFromCut(cut.getLeaves(), output);

		System.out.println("*****************************");
		a.toGraphvid();
		for(AndNode n: cut.getLeaves()) {
			for(AndNode out:n.getOutput()) {
				System.out.println(out.getId()+" -> "+out.getInput1().getId()+";");
				System.out.println(out.getId()+" -> "+out.getInput2().getId()+";");
			}
			
		}
		System.out.println("*****************************");
		
	}
	
	public void sizeOfCut(Cut cut, AndNode output) {
		AIG a = AIGRewriter.createSubgraphFromCut(cut.getLeaves(), output);
		System.out.println("Cut Nodes: "+a.size());
		
	}

	@Override
	public int compareTo(AIG o) {
		return this.id - o.id;
	}

	public void setId(int id) {
		this.id = id; 
	}
	
}
