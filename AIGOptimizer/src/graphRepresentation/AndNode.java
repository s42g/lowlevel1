package graphRepresentation;

import java.util.HashSet;

public class AndNode implements Node, Comparable<AndNode> {
	private int id;
	private AndNode in1;
	private AndNode in2;
	private HashSet<AndNode> out;
	private boolean inverted1;
	private boolean inverted2;

	public AndNode(int id, AndNode in1, AndNode in2, HashSet<AndNode> out, boolean inverted1, boolean inverted2) {
		this.id = id;
		this.in1 = in1;
		this.in2 = in2;
		if (out != null)
			this.out = out;
		else
			this.out = new HashSet<AndNode>();
		this.inverted1 = inverted1;
		this.inverted2 = inverted2;
	}

	public void setOutput(HashSet<AndNode> node) {
		out = node;
	}

	public HashSet<AndNode> getOutput() {
		return out;
	}

	public void setInputs(AndNode in1, AndNode in2) {
		this.in1 = in1;
		this.in2 = in2;
	}
	
	public void setInput1(AndNode in1) {
		this.in1 = in1;
	}
	
	public void setInput2(AndNode in2) {
		this.in2 = in2;
	}

	public AndNode getInput1() {
		return in1;
	}

	public AndNode getInput2() {
		return in2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isInverted1() {
		return inverted1;
	}

	public void setInverted1(boolean inverted1) {
		this.inverted1 = inverted1;
	}

	public boolean isInverted2() {
		return inverted2;
	}

	public void setInverted2(boolean inverted2) {
		this.inverted2 = inverted2;
	}

	@Override
	public String toString() {
		if (getInput1() != null && getInput2() != null)
			return "AndNode: " + id + ", In1: " +(isInverted1()?"-":"")+ getInput1().getId() + ", In2:" +(isInverted2()?"-":"")+ getInput2().getId();
		else
			return "AndNode: " + id;
	}

	public void addOutput(AndNode node) {
		out.add(node);
	}
	
	public void removeOutput(AndNode node) {
		out.remove(node);
	}

	@Override
	public int compareTo(AndNode o) {
		// TODO Auto-generated method stub
		return -this.getId()+o.getId();
	}
}
